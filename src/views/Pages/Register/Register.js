// eslint-disable-next-line no-unused-vars
import React, { Component } from 'react'
import { register } from '../components/UserFunctions'
// import { Button } from "reactstrap";
import jwt_decode from 'jwt-decode'
class Register extends React.Component {

  constructor() {
    super()
    this.state = {
      id: '',
      first_name: '',
      last_name: '',
      staff_phone: '',
      role: '',
      email: '',
      password: '',
      errors: {}
    }

    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }
  onSubmit = e => {
    e.preventDefault()

    const newUser = {
      role: 'staff',
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      staff_phone: this.state.staff_phone,
      email: this.state.email,
      password: this.state.password
    } 
      register(newUser).then(res => {
        this.props.history.push(`/`)
      })
  }
  componentDidMount() {
    const token = localStorage.usertoken
    const decoded = jwt_decode(token)
    this.setState({
      staff_id: decoded.staff_id,
      role: decoded.role
    })
  }
  render() {
    const adminLink = (
      <div className="row">
        <div className="col-md-6 mt-5 mx-auto">
            <form onSubmit={this.onSubmit}>
            <h1 className="h3 mb-3 font-weight-normal">เพิ่มบัญชีเจ้าหน้าที่</h1>
            <div className="form-group">
              <label htmlFor="name">ชื่อจริง</label>
              <input
                type="text"
                className="form-control"
                name="first_name"
                placeholder="ชื่อจริง"
                value={this.state.first_name}
                onChange={this.onChange}
                required
              />
            </div>
            <div className="form-group">
              <label htmlFor="name">นามสกุล</label>
              <input
                type="text"
                className="form-control"
                name="last_name"
                placeholder="นามสกุล"
                value={this.state.last_name}
                onChange={this.onChange}
                required
              />

            </div>
            <div className="form-group">
              <label htmlFor="phone">เบอร์โทร</label>
              <input
                type="number"
                className="form-control"
                name="staff_phone"
                placeholder="เบอร์โทร"
                value={this.state.staff_phone}
                onChange={this.onChange}
                required
              />
            </div>
            <div className="form-group">
              <label htmlFor="email">อีเมลล์</label>
              <input
                type="email"
                className="form-control"
                name="email"
                placeholder="อีเมลล์"
                value={this.state.email}
                onChange={this.onChange}
                required
              />
            </div>
            <div className="form-group">
              <label htmlFor="password">รหัสผ่าน</label>
              <input
                type="password"
                className="form-control"
                name="password"
                placeholder="รหัสผ่าน"
                value={this.state.password}
                onChange={this.onChange}
                required
              />
            </div>
            <button
              type="submit"
              className="btn btn-lg btn-primary btn-block"
            >
              เพิ่มบัญชี
              </button>
            </form>

        </div>
      </div>
    )
    const stafflink = (
      <div>
        <h1>เฉพาะแอดมินเท่านั้น</h1>
        <a href='#/home'>ย้อนกลับ</a>
      </div>
    )
    return (
      <div className="container">
        {this.state.role === 'admin' ? adminLink : stafflink}
      </div>
    )
  }
}

export default Register
