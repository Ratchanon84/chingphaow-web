/* eslint-disable jsx-a11y/anchor-is-valid */

import React, { Component } from "react";
// import { Link } from "react-router-dom";
// eslint-disable-next-line no-unused-vars
import { Map, GoogleApiWrapper, Marker, InfoWindow } from "google-maps-react";
import "./App.css";
// import "./css/default.css"
import logo from "./asset/chingphaow-removebg.png";
import cp1 from "./asset/cp1.jpg";
import cp2 from "./asset/cp2.jpg";
import flow from './asset/flow.jpg'
import { Form, Button, Image } from "react-bootstrap";
import { login } from "../components/UserFunctions";

class Landingpage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      markers: [],
      page: 1,
      seed: 1,
      refreshing: false,
      filterCrime: "",
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {},
      region: {
        latitude: 20.050470250943587,
        longitude: 99.87799879855217,
        latitudeDelta: 0.2922,
        longitudeDelta: 0.0421,
      },
      email: "",
      password: "",
      errors: {},
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  onSubmit(e) {
    e.preventDefault();

    const user = {
      email: this.state.email,
      password: this.state.password,
    };

    login(user).then((res) => {
      if (res) {
        this.props.history.push(`/home`);
        alert("เข้าสู่ระบบสำเร็จ!");
      } else {
        alert("Email หรือ Password ไม่ถูกต้อง กรุณาลองอีกครั้ง");
      }
    });
  }

  onMarkerClick = (props, marker, e) =>
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true,
    });

  onClose = (props) => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null,
      });
    }
  };

  fetchMarkerData() {
    fetch("https://chingphaow-application.herokuapp.com/requests/")
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          markers: responseJson.data,
          refreshing: false,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }
  componentDidMount() {
    this.fetchMarkerData();
  }

  render() {
    return (
      <body>
        <div class="tt">
          <nav
            class="w3-sidebar  w3-collapse w3-top w3-large w3-padding"
            id="mySidebar"
          >
            <br></br>

            <div class="w3-container">
              <Image className="logoStyle" src={logo} roundedCircle />
            </div>
            <br></br>
            <div class="w3-bar-block">
              <Form onSubmit={this.onSubmit}>
                <Form.Group controlId="formBasicEmail">
                  <Form.Label>อีเมลล์</Form.Label>
                  <input
                    type=""
                    className="form-control"
                    name="email"
                    placeholder="กรอกอีเมลล์"
                    value={this.state.email}
                    onChange={this.onChange}
                  />
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                  <Form.Label>รหัสผ่าน</Form.Label>
                  <input
                    type="password"
                    className="form-control"
                    name="password"
                    placeholder="กรอกรหัสผ่าน"
                    value={this.state.password}
                    onChange={this.onChange}
                  />
                </Form.Group>
                <Button variant="primary" type="submit" style={{marginLeft:90}}>
                เข้าสู่ระบบ
                </Button>
              </Form>
            </div>
          </nav>

          {/* sidebar on small screen */}
          <header class="sidebarSM w3-container  w3-hide-large  w3-xlarge w3-padding">
            <Form onSubmit={this.onSubmit}>
                <Form.Group controlId="formBasicEmail">
                  <Form.Label style={{fontSize:20}}>อีเมลล์</Form.Label>
                  <input
                    type=""
                    className="form-control"
                    name="email"
                    placeholder="กรอกอีเมลล์"
                    value={this.state.email}
                    onChange={this.onChange}
                  />
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                  <Form.Label style={{fontSize:20}}>รหัสผ่าน</Form.Label>
                  <input
                    type="password"
                    className="form-control"
                    name="password"
                    placeholder="กรอกรหัสผ่าน"
                    value={this.state.password}
                    onChange={this.onChange}
                  />
                </Form.Group>
                <Button variant="primary" type="submit">
                เข้าสู่ระบบ
                </Button>
              </Form>
          </header>

          {/* page ccontent */}
          <div class="tt w3-main">
            {/* <!-- Header --> */}
            <div class="w3-content" id="showcase">
            <h1>
                ระบบบริหารจัดการเชื้อเพลิงในพื้นที่ป่า
              </h1>
              <h3>แอพพลิเคชั่นชิงเผาเป็นแอพพลิเคชั่นสำหรับการจัดการเชื้อเพลิงการเผา ที่มีการติดตามกระบวนการก่อนเริ่มเผา กำลังเผา และหลังเผาเสร็จ
                โดยมีการคำนวณความเหมาะสมในการดำเนินการเผา อาศัย 3 ปัจจัยหลัก ได้แก่ ความชื้นสัมพัทธ์ในอากาศ,อุณหภูมิ และแรงลม
                อีกทั้งยังมีการรายงานสรุปผลในแต่ละพื้นที่ เพื่อสามารถจัดระเบียบการเผาได้อย่างเป็นระบบมากขึ้น มีบุคคลที่เกี่ยวข้องกับระบบดังนี้
              </h3>
              <h3>• ชาวบ้านหรือผู้นำหมู่บ้าน - สามารถส่งคำขอได้โดยการใส่ข้อมูลส่วนตัวชื่อ,ที่อยู่,เบอร์โทร,วันที่ และ ระบุตำแหน่งที่ต้องการได้</h3>
              <h3>• เจ้าหน้าที่ดำเนินการเผา - สามารถเห็นคำขอของชาวบ้านและสามารถอัพเดทสถานะของคำขอนั้นๆเพื่อเริ่มทำการเผาได้</h3>
              <h3>• ผู้ดูแลระบบ - สามารถดูข้อมูลรายงานและ จัดการข้อมูลผู้ใช้ และข้อมูลต่างๆของระบบ</h3>
              <br></br>
              <Image className="imgStyle" src={flow}/>
              <br></br>
              <br></br>
              <br></br>
              <h1>
                “ชิงเผา” ก่อนวันอันตราย
                และทางเลือกคืนลมหายใจบริสุทธิ์ให้เชียงใหม่
              </h1>
              <br></br>
              <h2>PM2.5 ในลมหายใจคนเชียงใหม่</h2>
              <h3>
                นับเป็นเวลาสิบกว่าปีมาแล้วที่จังหวัดเชียงใหม่และภูมิภาคเหนือบนประสบปัญหาวิกฤติหมอกควันซึ่งทำให้เกิดฝุ่นละอองขนาดเล็ก
                PM 2.5
                ซึ่งองค์การอนามัยโลกกำหนดไว้ว่าเป็นค่าที่เป็นอันตรายร้ายแรงต่อสุขภาพ
                และคนในภูมิภาคนี้ประสบกับภาวะเจ็บป่วยกับโรคทางเดินหายใจและโรคอื่นๆ
                อย่างหนักในทุกๆ ปีเมื่อเข้าหน้าแล้ง สำหรับมลพิษทางอากาศ PM2.5
                นี้ เป็นฝุ่นที่มีเส้นผ่าศูนย์กลางไม่เกิน 2.5 ไมครอน
                เกิดจากทั้งการเผาไหม้เครื่องยนต์พาหนะ
                การเผาวัสดุเศษพืชทางการเกษตร ไฟป่า และกระบวนการอุตสาหกรรม
                ซึ่งฝุ่นจิ๋วนี้สามารถเข้าไปถึงในถุงลม ในปอด
                และเข้าสู่กระแสโลหิตได้ ส่งผลให้เกิดโรคทางเดินหายใจ
                และโรคปอดต่างๆ ถ้าได้ปริมาณมากหรือสะสมต่อเนื่องเป็นเวลานาน
                ก็อาจเป็นสาเหตุของโรคมะเร็งปอด หรือโรคหัวใจ หลายปีที่ผ่านมา
                ภูมิภาคเหนือบนได้สรุปสาเหตุหมอกควันพิษเหล่านี้ว่า
                มีสาเหตุหลักมาจากการเผาไหม้ไฟป่าหรือการเศษวัสดุธรรมชาติทางการเกษตร
                รวมถึงมีหมอกควันข้ามพรมแดนจากประเทศเพื่อนบ้านซึ่งปลูกพืชเศรษฐกิจเชิงเดี่ยวเช่นกัน
                จึงทำให้ฤดูกาลช่วงนี้ (กุมภาพันธ์- เมษายน)
                ปรากฏหมอกควันอันเกิดจากการเผาไหม้อย่างหนัก
                จนเป็นที่มาของนโยบายห้ามเผาโดยเด็ดขาดในเขตประเทศไทย
                ส่วนประเทศเพื่อนบ้านเราทำได้เพียงขอความร่วมมือลดการเผา
                การออกกฎหมายห้ามเผาโดยเด็ดขาดนี้ทำให้นอกจากเกษตรกรจำนวนไม่น้อยต้องเดือดร้อน
                เนื่องเพราะพื้นที่การเกษตรบางแห่งมีความจำเป็นต้องเผาแล้ว
                ยังทำให้ยามเกิดไฟป่าแต่ละครั้ง เกิดการลุกไหม้อย่างหนัก
                เนื่องเพราะมีการสะสมของเชื้อเพลิงในป่า จนทำให้ทุกๆ ปี
                แม้จะมีมาตรการห้ามเผา แต่ภาวะฝุ่นพิษ PM2.5 ก็ยังเกิดขึ้นทุกครั้ง
                โดยเฉพาะในอำเภอเมืองเชียงใหม่จะประสบปัญหาหนักกว่าที่อื่น
              </h3>
              <br></br>
              <h3>
                <b>
                  ด้วยเหตุนี้จึงมีหลายฝ่ายหลายองค์กรที่พยายามช่วยกันแสวงหาทางออกร่วมกันเพื่อบรรเทาภาวะฝุ่นพิษคลุมเมืองเชียงใหม่ให้อยู่สถานะที่เกิดผลกระทบให้น้อยที่สุด
                </b>
              </h3>
              <Image className="imgStyle" src={cp1} />
              <br></br>
              <br></br>
              <h3>
                นับตั้งแต่เกิดวิกฤติหมอกควันในเมืองเชียงใหม่และมีมาตรการห้ามเผาเกิดขึ้นในภูมิภาคเหนือบน
                ชาวบ้านแม่เตี๊ยะใต้ก็ปฏิบัติตามคำสั่งด้วยดี
                แต่พวกเขาก็พบว่าไฟป่าก็ไม่ได้ลดลงเลย ยังคงเกิดขึ้นทุกปี
                บางปีไฟก็ลุกลามอย่างหนัก
                เวลาที่ทางหน่วยงานป่าไม้ขอความร่วมมือชาวบ้านมาเป็นอาสาสมัครดับไฟป่า
                การดับไฟแต่ละครั้งก็ไม่ได้ง่าย
                บางครั้งก็ได้ยินข่าวการเสียชีวิตของอาสาสมัครอันเกิดจากการปฏิบัติงานดับไฟ
                พ่อหลวงจึงเห็นว่า มาตรการห้ามเผาเสียทีเดียว น่าจะใช้ไม่ได้
                เพราะมันคือการสะสมเชื้อเพลิง
                ถ้าเรากำจัดเชื้อเพลิงก่อนบางส่วนในช่วงเวลาที่เหมาะสมน่าจะเป็นผลดีมากกว่า
                เพราะดูจากการปฏิบัติของคนรุ่นก่อน ยามพวกเขาเข้าป่า
                จะมีการเผาไฟไปก่อนบ้างเล็กๆ น้อยๆ และทำแนวกันไฟ
                พอถึงช่วงฤดูไฟป่าจริงๆ ก็มักจะเสียหายไม่มาก
                “ชาวบ้านเราซึ่งเป็นอาสาสมัครขึ้นไปดับ จะเจอปัญหาเยอะมาก
                ทั้งเรื่องความชัน ทิศทางลม บางทีก็เอาไม่อยู่ และเราก็จะเห็น
                จุดที่เกิดไฟไหม้บ่อย เกิดซ้ำๆ ทุกปี
                ก็เลยคิดว่าเราไปเผาก่อนและควบคุมไฟให้มันถูกเผาในปริมาณที่พอดีๆ
                ไม่ได้เผาไปหมด ทำแนวกันไฟไปด้วย
                อย่างน้อยที่สุดตอนที่มันเกิดไฟป่าในช่วงที่เราไม่สามารถควบคุมได้
                มันจะได้ไม่ไหม้หนัก ไฟป่าจะเกิดขึ้นน้อยถ้าเชื้อเพลิงมีน้อย
                และชาวบ้านเราก็ไม่ต้องเสี่ยงอันตรายมาก”
              </h3>
              <Image className="imgStyle" src={cp2} />
              <br></br>
            </div>
          </div>
        </div>
      </body>
    );
  }
}
// eslint-disable-next-line no-unused-vars
const mapStyles = {
  width: "100%",
  height: "100%",
};

export default GoogleApiWrapper({
  apiKey: "AIzaSyAyir1wNaHCINAHUmm_4e-8hpICN3Q6z2U",
})(Landingpage);
